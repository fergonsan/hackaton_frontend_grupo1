import { LitElement, html, css } from "lit-element";

class CardFichaListado extends LitElement{

    static get properties(){
        return{
            pan: {type: String},
            cif: {type: String},
            dni: {type: String},
            cantidad: {type: Number},
            nombre: {type:String},
            movs: {type:Array}
        };
    }

    constructor(){
        super();      
        this.movs=[]      
    }

    static get styles() {
        return css`
            .card-text{
                color: #072146;
                display: inline;
            }
            .card-header{
                color: #072146;

            }
            h5{
                color: #072146;
                display: inline;
            }
            .movimientos{            
                display: flex;
                flex-wrap: wrap;                
            }           
          `;
      }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            
            <div id="cardList" class="card h-100 text-dark bg-light mb-3" style="max-width: 18rem;">
                <img src="img/profile.png" alt="profile" class="card-img-top">
                <div class="card-header"><strong>Tarjeta: </strong>${this.pan}</div>
                <div class="card-body">
                    <div class="row h-70"><h5 class="card-title">${this.nombre}</h5></div>
                        <div class="row h-100">                      
                            <p class="card-text">Puntos: <span class="badge bg-pill btn-success me-3">${this.cantidad}</span>                           
                            <button class="btn btn-success col-2" @click="${this.addSaldo}" ><strong>+</strong></button>
                            <button class="btn btn-danger col-2" @click="${this.removeSaldo}" ><strong>-</strong></button>                  
                            </p>      
                        </div>
                    </div>                    
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Fecha</th>
                                <th scope="col">Concepto</th>
                                <th scope="col">Cantidad</th>                            
                            </tr>
                        </thead>
                        <tbody> 
                        ${this.movs.map(
                            mov => html`<tr>                                                        
                            <td>${mov.fecha}</td>
                            <td>${mov.concepto}</td>
                            <td>${mov.cantidad}</td>
                            </tr>`
                        )} 
                        </tbody>
                    </table>
                </div>
                <div class="card-footer ">
                    <button class="btn btn-danger col-5" @click="${this.deleteCard}"><strong>Borrar</strong></button>
                    <button class="btn btn-info offset-1 col-5 d-none" @click="${this.moreInfo}"><strong>Puntos</strong></button>                    
                </div>
            </div>          
            
        `;
    }

    updated(changedProperties){
        //console.log("changedProperties card-ficha-listado");
        //console.log(changedProperties)
        if (changedProperties.has("dni")){
            if(this.dni!=""){
                this.getUser(this.dni);
            }else{
                this.shadowRoot.getElementById("cardList").classList.add("d-none")
            }
        }        
    }
    
   addSaldo(){
        //console.log("addSaldo")
        this.dispatchEvent(new CustomEvent("add-saldo",{detail: {
                                                pan: this.pan,
                                                dni: this.dni
                                            }
                                        }))
    }

    removeSaldo(){
        //console.log("removeSaldo")
        this.dispatchEvent(new CustomEvent("remove-saldo",{detail: {
                                                pan: this.pan,
                                                dni: this.dni
                                            }
                                        }))
    }

    getUser(dni){
        let xhr = new XMLHttpRequest();
        xhr.onload = ()=> {            
            if (xhr.status === 200) {                
                let APIResponse = JSON.parse(xhr.responseText);
                this.nombre = APIResponse.user.nombre
            }
        }
        xhr.open("GET", "http://localhost:8080/loyalty/v1/user/"+dni);
        xhr.send();       
    }
   
    deleteCard(e){
        //console.log("deleteCard");
        //console.log("Se va a borrar la tarjeta con PAN: "+this.pan);
        this.dispatchEvent(
            new CustomEvent("delete-card",{
                    detail: {
                        pan: this.pan
                    }
                }
            )
        );                      
    }
}

customElements.define("card-ficha-listado", CardFichaListado);