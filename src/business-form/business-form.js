import {LitElement, html, css } from 'lit-element';

class BusinessForm extends LitElement {

static get properties() {

    return {
        company: {type: Object},
        showFormBusiness: {type: Boolean},
        showErrorForm:{type:Boolean}
    };
}

constructor(){
    super();
    this.showErrorForm=false;
    this.resetFormData();
}
static get styles() {
    return css` 
      .loclabels{
          font-size: 20px;
          color: #072146;
      }
             
      `;
  }
    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group pb-3">
                    <label class="loclabels">Nombre Completo <FONT COLOR="red">*</FONT></label>
                    <input @input="${this.updateName}" 
                        .value="${this.company.name}"                         
                        type="text" class="form-control" placeholder="Nombre de la Empresa" />
                </div>
                <div class="form-group pb-3">
                    <label class="loclabels">Cif <FONT COLOR="red">*</FONT></label>
                    <input type="text" @input="${this.updateCif}" .value="${this.company.cif}" class="form-control" placeholder="Cif de la Empresa"></textarea>
                </div>
                <div class="form-group pb-3">
                    <label class="loclabels">Dirección de la Empresa</label>
                    <input type="text" @input="${this.updateDir}" .value="${this.company.dir}"  class="form-control" placeholder="Dirección de la Empresa" />
                </div>
                <div class="text-danger mb-3"><em>* Campo Obligatorio</FONT></em></div>
                <div>
                    <label id="Error" class="d-none mb-3 text-danger">Error: los campos CIF y Nombre no pueden estar vacíos</label>
                </div>
                <button @click="${this.goBack}" class="btn btn-danger"><strong>Atrás</strong></button>
                <button @click="${this.storeCompany}" class="btn btn-success"><strong></strong>Guardar</button>
                
            </form>
        </div>
        `;
    }

    updated(changedProperties){        
        if(changedProperties.has("showErrorForm")){
            if (this.showErrorForm===false){
                this.hideErrorLabel();
            }else if(this.showErrorForm===true){
                this.showErrorLabel();
            }
        }
    }
    showErrorLabel(){        
        this.shadowRoot.getElementById("Error").classList.remove("d-none")
    }
    
    hideErrorLabel(){
        //console.log("hideErrorLabel")
        this.shadowRoot.getElementById("Error").classList.add("d-none")
    }

    updateName(e) {
        //console.log("updateName");
        //console.log("Actualizando el valor de la propiedad name de company con valor " + e.target.value);

        this.company.name = e.target.value;
    }

    updateCif(e) {
        //console.log("updateCif");
        //console.log("Actualizando el valor de la propiedad cif de company con valor " + e.target.value);
        this.company.cif = e.target.value;
    }

    updateDir(e) {
        //console.log("updateDir");
        //console.log("Actualizando el valor de la propiedad dir de company con valor " + e.target.value);
        this.company.dir = e.target.value;
    }

    goBack(e) {
        //console.log("goBack");
        //console.log("Se ha cerrado el formulario de empresa");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("company-form-close", {}));
    }

    storeCompany(e) {
        //console.log("storeCompany");
        //console.log("Se va a almacenar una empresa");
        e.preventDefault();

        //console.log("La propiedad name de company vale " + this.company.name);
        //console.log("La propiedad profile de company vale " + this.company.cif);
        //console.log("La propiedad YearsInCompany de company vale " + this.company.dir);

        this.dispatchEvent(new CustomEvent("company-form-store", {
            detail: {
                company: {
                    name: this.company.name,
                    cif: this.company.cif,
                    dir: this.company.dir,
                },
                showFormBusiness: this.showFormBusiness
            }
        }))
    };

    resetFormData() {
    //console.log("resetFormData");
    this.company = {};
    this.company.name = "";
    this.company.cif = "";
    this.company.dir = "";
    
    this.showFormBusiness = false;
    this.showErrorForm=false;
    }
}

customElements.define("business-form", BusinessForm);