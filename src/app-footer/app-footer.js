import {LitElement, html, css} from 'lit-element'


class AppFooter extends LitElement {


    static get properties(){
        return {

        }
    }

    constructor() {
        super()
    }

    static get styles() {
      return css` 
        .footer{
          text-align: center;
          color: white;
          max-width:100%;
          height:80px ;
          align-items: center;
          background-color: #072146;
        }        
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">            
        <div class="footer pt-4"><h5>© bLOC App 2021</h5></div>            
        `
    }
}

customElements.define('app-footer', AppFooter)