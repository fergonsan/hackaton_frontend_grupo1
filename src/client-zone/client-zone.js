import {LitElement, html, css} from 'lit-element'

class ClientZone extends LitElement {


    static get properties(){
        return {

        }
    }

    constructor() {
        super();

    }

    static get styles() {
      return css`        
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <div> Hola Cliente </div>
        `
    }   
}

customElements.define('client-zone', ClientZone)