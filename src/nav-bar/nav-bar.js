import {LitElement, html, css} from 'lit-element'
import "../business-zone/business-zone.js";
import "../client-zone/client-zone.js"

class NavBar extends LitElement {


    static get properties(){
        return {
            showClient:{type:Boolean},
        }
    }

    constructor() {
        super();
        this.showClient=true;

    }

    static get styles() {
      return css`        
        .color-bbva{
            background-color:#072146;            
        }        
        .col-auto{
            height: 85vh;            
        }
        .nav{
            background-color:#072146;
        }      
        .nav-item{
            color:white;            
            margin:10px;          
        }
        .btn-nav {            
            border-color:  #072146;
            background-color: #072146;
            color:  #ffffff;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-left: 30px;
            padding-right: 20px;
        }
        .btn-nav:hover,
        .btn-nav:focus {
            border-color:  #ffffff;
            background-color:  #ffffff;
            color: #072146;
        }  
                     
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <aside>
            <section>
            <div class="container-fluid gx-1">
                <div class="row flex-nowrap">
                    <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 color-bbva">
                        <ul class="nav flex-column ms-2">
                            <li>
                                <button class="btn btn-nav fs-5" @click="${this.mainPage}" type="button">Inicio</button>
                            </li>
                            <li>
                                <button class="btn btn-nav fs-5"   @click="${this.iAmBusiness}" type="button">Soy Empresa</button>
                            </li>
                            <li>
                                <button class="btn btn-nav fs-5 disabled" @click="${this.iAmClient}" type="button">Soy Particular</button>
                            </li>                   
                        </ul>
                    </div>
                    <div class="col py-3">
                        <client-zone class="d-none"></client-zone>
                        <business-zone></business-zone>
                       
                    </div>
                </div>
            </div>
            </section>
        </aside>          
        `
    }
    updated(changedProperties){
        //console.log("updated en nav bar");
        //console.log(changedProperties)
        if(changedProperties.has("showClient")){
            //console.log("ha cambiado el valor de la propiedad showClient en nav-bar");
            if (this.showClient === true){
                this.showClientZone();
            }else if (this.showClient === false){
                //console.log("Mostrar la zona de business "+this.showClient)
                this.showBusinessZone();
            }
        }        
    }
    mainPage(e){
        //console.log("mainPage");
        this.dispatchEvent(new CustomEvent("main-page",{}));
    }

    iAmBusiness(e){
        //console.log("iAmBusiness");        
        this.dispatchEvent(new CustomEvent("nav-business-zone",{}));
    }
    iAmClient(e){
        //console.log("iAmClient");
        //console.log("Entrando en la zona de particulares")
        this.dispatchEvent(new CustomEvent("nav-client-zone",{}));
    }
    showClientZone(){
        //console.log("showClientZone nav-bar");
        this.shadowRoot.querySelector("client-zone").classList.remove("d-none");
        this.shadowRoot.querySelector("business-zone").classList.add("d-none");
        this.shadowRoot.querySelector("business-zone").showFormBusiness=false;
    }
    showBusinessZone(){
        //console.log("showBusinessZone nav-bar");
        this.shadowRoot.querySelector("client-zone").classList.add("d-none");
        this.shadowRoot.querySelector("business-zone").classList.remove("d-none");
        this.shadowRoot.querySelector("business-zone").showFormBusiness=false;
        this.shadowRoot.querySelector("business-zone").showFormBusiness=true;
    }

}

customElements.define('nav-bar', NavBar)