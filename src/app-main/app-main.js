import {LitElement, html, css} from 'lit-element'
import "../button-main/button-main.js"

class AppMain extends LitElement {


    static get properties(){
        return {
            showNavBar: {type:Boolean}
        }
    }

    constructor() {
        super();
        

    }

    static get styles() {
      return css`               
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>    
        <div> 
            <div class="row d-flex justify-content-center">           
                <img src="img/portada.jpg">                   
            </div>                                    
            <div class="row">
                <button-main @client-zone="${this.clientZone}" @business-zone="${this.businessZone}"></button-main>             
            </div>
        </div>     
        `
    }

    
    clientZone(){        
        this.dispatchEvent(new CustomEvent("show-client-zone",{}));
    }

    businessZone(){
        this.dispatchEvent(new CustomEvent("show-business-zone",{}));
    }    
}

customElements.define('app-main', AppMain)