import {LitElement, html, css} from 'lit-element'


class AppHeader extends LitElement {


    static get properties(){
        return {

        }
    }

    constructor() {
        super()
    }

    static get styles() {
      return css` 
        #logo{
          max-width:100%;
          height:100px ;
          display: flex;
          align-items: center;
          background-color: #072146;
        }

        #image{
          margin-left: 10px;
          height: 50px;
        }
        `;
    }
    
    render(){
        return html`            
            <div id="logo">
              <img id="image" src="../img/logo_large.png" >
            </div>            
        `
    }
}

customElements.define('app-header', AppHeader)