import {LitElement, html, css} from 'lit-element'
import "../card-ficha-listado/card-ficha-listado.js"

class BusinessList extends LitElement {


    static get properties(){
        return {
            company:{type:Object},
            cardList:{type:Array},
            resetForm:{type:Boolean}
        }
    }

    constructor() {
        super();
        this.company=[];
        this.cardList=[];

    }

    static get styles() {
      return css`
        h5{
            color: #072146;
            display: inline;
        } 
        .col-1,
        .col-4,
        .col-5,
        .col-11{
            align-self: center;
        }        
        `;
    }
    
    render(){
        return html`  
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
                    rel="stylesheet" 
                    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
                    crossorigin="anonymous">  
        
        <button type="button" class="btn btn-link" @click="${this.goBack}">Volver</button>
        <div class="container ms-5">
                <div class="row">
                    <div class="col-1">
                        <img src="img/genericCompany.png" alt="genericCompany"  class="card-img-top">
                    </div>
                    <div class="col-11">
                        <div class="row">
                            <div class="col-1"><h5><strong>Empresa: </strong></h5> </div>
                            <div class="col-5 ms-3">${this.company.nombre}</div>
                        </div>
                        <div class="row">
                            <div class="col-1"><h5><strong>CIF: </strong></h5> </div>
                            <div class="col-5 ms-3">${this.company.cif}</div>
                            <div class="col-2"><button type="button" class="btn btn-danger" @click="${this.deleteCompany}">Eliminar</button></div>
                        </div>
                        <div class="row">
                            <div class="col-1"><h5><strong>Dirección: </strong></h5> </div>
                            <div class="col-5 ms-3">${this.company.direccion}</div>
                        </div>
                    </div>
                </div>
                <div class="row pt-4" id="cardList">
                        <div class="row row-cols-1 row-cols-sm-4">
                            ${this.cardList.map(
                                card => html`<card-ficha-listado 
                                pan=${card.pan} 
                                cif=${card.cif} 
                                dni=${card.dni} 
                                cantidad=${card.cantidad}
                                .movs=${card.movs}
                                nombre=${card.nombre}
                                @delete-card=${this.deleteCard}
                                @add-saldo=${this.addSaldo}
                                @remove-saldo=${this.removeSaldo}
                                </card-ficha-listado>`
                            )}                
                        </div>
                    </div>            
        </div>
        `
    }

    goBack(e){
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("business-list-close", {
        }));
    }

    updated(changedProperties){
        if(changedProperties.has("company")){
            this.getCardList();
        }
        if (changedProperties.has("resetForm")){
            this.resetData();
        }          
    }

    resetData(){
        this.company=[];
        this.cardList=[];
        this.resetData=false;
    }

    getCardList(){
        let xhr = new XMLHttpRequest();
        xhr.onload = ()=> {            
            if (xhr.status === 200) {                
                let APIResponse = JSON.parse(xhr.responseText);
                let cardListAux = APIResponse.filter(card => card.cif === this.company.cif)
                cardListAux.map(card =>{                                
                    if(card.movs === null){
                    card.movs=[];
                    let mov= new Object();
                    mov.fecha=""
                    mov.concepto=""
                    mov.cantidad=""
                    card.movs.push(mov)
                }})
                this.cardList=cardListAux
            }   
        };
        xhr.open("GET", "http://localhost:8080/loyalty/v1/card");
        xhr.send();
    }

    deleteCard(e){
        let xhr = new XMLHttpRequest();
        xhr.onload = ()=> {            
            if (xhr.status === 200) {                
                let APIResponse = JSON.parse(xhr.responseText);
                this.cardList=this.getCardList();               
            }
        }
        xhr.open("DELETE", "http://localhost:8080/loyalty/v1/card/"+e.detail.pan);
        xhr.send();
    }

    deleteCompany(e) {
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("delete-company", {
            detail: {
                cif: this.company.cif,
                cardList: this.cardList
                }
            }
        ));
    }

    addSaldo(e){
        //console.log("addSaldo en business-list")  
        var xhr = new XMLHttpRequest();        
        xhr.open("PATCH", "http://localhost:8080/loyalty/v1/card");        
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        let movs= []
        let mov= new Object();
            mov.fecha="11-06-2021"
            mov.concepto=this.company.nombre
            mov.cantidad=1
            movs.push(mov)
        let data = new Object();
            data.pan=e.detail.pan;
            data.dni=e.detail.dni;
            data.cif=this.company.cif;
            data.cantidad=0
            data.movs=movs    
        var StringData = JSON.stringify(data)       
        xhr.send(StringData)
        this.getCardList();
    }

    removeSaldo(e){
        //console.log("removeSaldo en business-list")     
        var xhr = new XMLHttpRequest();        
        xhr.open("PATCH", "http://localhost:8080/loyalty/v1/card");        
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        let movs= []
        let mov= new Object();
            mov.fecha="11-06-2021"
            mov.concepto=this.company.nombre
            mov.cantidad=-1
            movs.push(mov)
        let data = new Object();
            data.pan=e.detail.pan;
            data.dni=e.detail.dni;
            data.cif=this.company.cif;
            data.cantidad=0
            data.movs=movs   
        var StringData = JSON.stringify(data)       
        xhr.send(StringData)
        this.getCardList();
    }
    
}

customElements.define('business-list', BusinessList)