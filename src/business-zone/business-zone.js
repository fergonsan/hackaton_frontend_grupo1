import {LitElement, html, css} from 'lit-element'

import "../business-list/business-list.js"
import "../business-form/business-form.js"

class BusinessZone extends LitElement {
    static get properties(){
        return {
            company:{type:Object},
            showFormBusiness:{type:Boolean}
        }
    }
    constructor() {
        super();
        this.resetFormData();
        this.showFormBusiness=true;
        this.company=[];

    }

    static get styles() {
      return css` 
        .form-label{
            font-size: 30px;
            color: #072146;
        }
        .btn-primary {            
            border-color:  #072146;
            background-color: #ffffff;
            color:  #072146;
        }
        .btn-primary:hover,
        .btn-primary:focus {
            border-color:  #072146;
            background-color:  #072146;
            color: #ffffff;
        }       
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <form id="formBusiness">
            <div class="row">
                <label class="form-label mt-5 ms-5">Introduzca el CIF:</label>
            </div>
            <div class="row">
                <div class="col-5">
                    <input @input="${this.updateCif}" .value="${this.company.cif}" type="text" class="form-control mt-2 ms-5" id="cifInput" placeholder="CIF">
                </div>
                <div class="col-4">
                    <button @click="${this.searchCompany}" class="btn btn-primary mt-2 ms-5">Enviar</button>
                </div>
            </div>            
        </form>
        <div id="AddCompany" class="row">
            <div class="col-5">
                <button @click="${this.showCompanyFormData}" class="btn btn-primary mt-2 ms-5">Añadir Empresa</button>
            </div>        
            <div class="row mt-5 ms-4 ml-2"> 
                <div class="col-5">               
                    <business-form 
                        @company-form-close="${this.companyFormClose}" 
                        @company-form-store="${this.companyFormStore}"
                        class="d-none rounded" id="companyForm"></business-form>
                </div>
            </div>
        </div>
        </div>
        <business-list class="d-none" @business-list-close="${this.businessListClose}" 
                                        @delete-company="${this.deleteCompany}"></business-list>
        `
    }   
    
    updated(changedProperties){
        //console.log("updated business-zone");
        if(changedProperties.has("showFormBusiness")){
            //console.log("updated showFormBusiness")
            if (this.showFormBusiness === true){
                this.showBusinessForm();
            }else if (this.showFormBusiness === false){
                this.showBusinessList();
            }
        }                
    }    
    updateCif(e){
        this.company.cif = e.target.value;
    }

    searchCompany(e){        
        e.preventDefault();
        let xhr = new XMLHttpRequest();
        xhr.onload = ()=> {            
            if (xhr.status === 200) {                
                let APIResponse = JSON.parse(xhr.responseText);                   
                this.shadowRoot.querySelector("business-list").company = APIResponse.company;
            }
        };        
        xhr.open("GET", "http://localhost:8080/loyalty/v1/company/"+this.company.cif);
        xhr.send();        
        this.showBusinessList();
    } 
    
    resetFormData(){        
        this.company= {};
        this.company.cif="";
    }
    businessListClose(e){  
        this.showFormBusiness=false;   
        this.showFormBusiness=true;        
    }
    showBusinessList(){
        //console.log("showBusinessList")        
        this.shadowRoot.getElementById("formBusiness").classList.add("d-none");
        this.shadowRoot.querySelector("business-list").classList.remove("d-none");
        this.shadowRoot.getElementById("AddCompany").classList.add("d-none");
    }
    showBusinessForm(){
        //console.log("showBusinessForm")        
        this.resetFormData();
        this.shadowRoot.getElementById("formBusiness").classList.remove("d-none");
        this.shadowRoot.querySelector("business-list").classList.add("d-none");
        this.shadowRoot.getElementById("AddCompany").classList.remove("d-none");
    }
    companyFormClose() {
        //console.log("companyFormClose");
        //console.log("Se ha cerrado el formulario de Empresa");        
        this.shadowRoot.querySelector("business-form").classList.add("d-none");        
    }

    companyFormStore(e) {
        /*console.log("companyFormStore");
        console.log("Se va a guardar la ficha de la nueva empresa");
        console.log("La propiedad name de company vale " + e.detail.company.name);
        console.log("La propiedad cif de company vale " + e.detail.company.cif);
        console.log("La propiedad dir de company vale " + e.detail.company.dir);        
        console.log("Se va a almacenar una empresa nueva");
        */
        e.preventDefault();
        var xhr = new XMLHttpRequest();  
        xhr.onload = ()=> {            
            if (xhr.status === 200) {
                this.shadowRoot.querySelector("business-form").classList.add("d-none");
            }else if (xhr.status === 400) {                               
                this.shadowRoot.querySelector("business-form").showErrorForm=true;
            }
        };      
        xhr.open("POST", "http://localhost:8080/loyalty/v1/company/");        
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        let company = new Object();
            company.nombre=e.detail.company.name;
            company.cif=e.detail.company.cif;
            company.direccion=e.detail.company.dir;          
        var StringData = JSON.stringify(company)                       
        xhr.send(StringData)
    }
    showCompanyFormData() {
        //console.log("showCompanyFormData");
        //console.log("Mostrando el formulario de Empresa");
        this.shadowRoot.querySelector("business-form").classList.remove("d-none");
    }
    deleteCompany(e) {
        //console.log("deleteCompany");
        //console.log("Se van a borrar los datos de la Empresa " + e.detail.cif);       
        //console.log("Borrando Tarjetas de esa empresa")
        e.detail.cardList.map(
            card => {
                let xhr = new XMLHttpRequest();
                xhr.onload = ()=> {            
                    if (xhr.status === 200) {                
                        let APIResponse = JSON.parse(xhr.responseText);                            
                    }
                }                
                xhr.open("DELETE", "http://localhost:8080/loyalty/v1/card/"+card.pan);
                xhr.send();
            }
        )
        let xhr = new XMLHttpRequest();
        xhr.onload = ()=> {            
            if (xhr.status === 200) {                
                let APIResponse = JSON.parse(xhr.responseText);                            
            }
        }                
        xhr.open("DELETE", "http://localhost:8080/loyalty/v1/company/"+e.detail.cif);
        xhr.send(); 
        this.shadowRoot.querySelector("business-list").resetForm=true;
        this.showBusinessForm();   
    }
}

customElements.define('business-zone', BusinessZone)