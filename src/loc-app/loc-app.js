import { LitElement, html, css } from "lit-element";

import "../app-header/app-header.js"
import "../app-footer/app-footer.js"
import "../app-main/app-main.js"
import "../nav-bar/nav-bar.js"

class LocApp extends LitElement {
     
    render () {

        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous" />
            <app-header></app-header>
            
            <div class="row">
                <div><nav-bar class="d-none" @main-page="${this.showMainPage}" @nav-business-zone="${this.showBusinessZone}" @nav-client-zone="${this.showClientZone}"></nav-bar></div>
                <div><app-main @show-client-zone="${this.showClientZone}" @show-business-zone="${this.showBusinessZone}"></app-main></div>
            </div>                
            <app-footer></app-footer>
        `;

    }

    showBusinessZone(){
        //console.log("showBusinessZone");
        //console.log("Zona de empresas con navbar");
        this.shadowRoot.querySelector("app-main").classList.add("d-none");
        this.shadowRoot.querySelector("nav-bar").classList.remove("d-none");
        this.shadowRoot.querySelector("nav-bar").showClient = true;                       
        this.shadowRoot.querySelector("nav-bar").showClient = false;        
    }

    showClientZone(){
        //console.log("showClientZone");
        //console.log("Zona de clientes con navbar");
        this.shadowRoot.querySelector("app-main").classList.add("d-none");
        this.shadowRoot.querySelector("nav-bar").classList.remove("d-none");
        this.shadowRoot.querySelector("nav-bar").showClient = true;
    }

    showMainPage(){
        //console.log("showMainPage");
        this.shadowRoot.querySelector("app-main").classList.remove("d-none");
        this.shadowRoot.querySelector("nav-bar").classList.add("d-none");
    }
}

customElements.define('loc-app', LocApp)