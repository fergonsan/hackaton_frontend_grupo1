import {LitElement, html, css} from 'lit-element'

class ButtonMain extends LitElement {


    static get properties(){
        return {
            profile:{type:String}
        }
    }

    constructor() {
        super();
    }

    static get styles() {
      return css`        
        .btn-primary,
        .btn-secondary {            
            border-color:  #072146;
            background-color: #ffffff;
            color:  #072146;
            padding-top: 20px;
            padding-bottom: 20px;
            padding-left: 100px;
            padding-right: 100px;
        }
        .btn-primary:hover,
        .btn-primary:focus {
            border-color:  #072146;
            background-color:  #072146;
            color: #ffffff;
        }             
        `;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        <div class="row justify-content-center">  
            <div class="col-12 col-md-auto">      
                <button class="btn btn-primary btn-lg mt-3 me-3 mb-3" @click="${this.iAmBusiness}" role="button">Soy Empresa</button> 
            </div>
            <div class="col-md-auto">      
                <button class="btn btn-secondary btn-lg mt-3 ms-3 mb-3 disabled" @click="${this.iAmClient}" role="button" >Soy Particular</button>
            </div>
        </div>
        `
    }

    iAmClient(e){
        //console.log("iAmClient");
        //console.log("Entrando en la zona de particulares")
        this.dispatchEvent(new CustomEvent("client-zone",{}));
    }

    iAmBusiness(e){
        //console.log("iAmBusiness");
        //console.log("Entrando en la zona de empresas")
        this.dispatchEvent(new CustomEvent("business-zone",{}));
    }
}

customElements.define('button-main', ButtonMain)